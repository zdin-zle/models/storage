# Storage Model ready to work with Mosaik

This repository provides a simple (electrical/hydrogen) storage model which is designed to be run with the co-simulation framework mosaik

## Electrical Storage
The eStorage class is designed to model a lithium-ion battery storage. Dynamic charging/discharging efficiency curves are used to recreate empirical behavior. The curves and resulting etas are derived from measurements (according to ["Effizienzleitfaden für PV-Speichersysteme"](https://www.bves.de/wp-content/uploads/2017/04/Effizienzleitfaden_V1.0.4_April2017.pdf)) within the elenia Institute at TU Braunschweig as well as from [PerModAC](https://pvspeicher.htw-berlin.de/veroeffentlichungen/daten/permodac/)from HTW Berlin and then poliniomial fitted using the MATLAB fitting Toolbox.

**Important**: The storage model is designed to be controlled by the ZLE-EMS (ZLE's Smart Building Energy Managment System) to determine the energy which needs to be stored / discharged in every time step. 

## Graphical Description: Overview
The following graphical illustration represents an overview of the implementation of the storage model and its dependence from the Energy Management.

![](docs/UML_storage_overview.png)

## Graphical Description: Electrical Storage
Following soon.

## Hydrogen Storage
The hStorage class for hydrogen storage is still under development - release ~mid 2022.

