"""
Mosaik interface for the Storage simulator.

@author: Christian Reinhold and Henrik Wagner elenia@TUBS

@review: Fernando Penaherrera @OFFIS/UOL

"""

import mosaik_api
from .storage import StorageModels

# SIMULATION META DATA
META = {
    'type': 'time-based',
    'models': {
        'eStorage': {
            'public': True,
            'params': ['init_vals'],
            'attrs': [
                'P',                # Active Power [W]
                'Q',                # Reactive Power [W]
                'P_SET',            # Set-Point - Battery Active Power 
                                    # (+ Charge, - Discharge) [W]
                'P_CHARGE_MAX',     # Current maximal active charge power [W] 
                'P_DISCHARGE_MAX',  # Current maximal active discharge power [W]
                'SOC',              # Current SOC - State of Charge [-]     
                'E_BAT',            # Current charging level [Wh]
                'P_SET_LIMIT',      # Limitation of Set Point [W]
                'BAT_CYCLES',       # Current state of health of battery [-]
                'SOH',              # Current state of health of battery [-]
                ],
        },
    },
    "extra_methods": ["get_entities"]}

# Mosaik API


class StorageSimulator(mosaik_api.Simulator):
    '''
    Mosaik API Simulator for accessing the storage models
    '''
    def __init__(self):
        super(StorageSimulator, self).__init__(META)
        # assign properties
        self.step_size = None

        # create dict for entities
        self.entities = {}

        # init Simulator of models
        self.simulator = StorageModels()

    # init mosaik API
    def init(self, sid, time_resolution=600, step_size=1):
        '''
        Initializazion of the entities
        
        :param sid: String ID (model name
        :param time_resolution: Resolution, seconds
        :param step_size: Modelling step size, seconds
        '''
        # assign properties
        self.sid = sid
        self.step_size = step_size

        # return meta data
        return self.meta

    # create models
    def create(self, num, model_type, init_vals):
        '''
        Creation of the model instances
        
        :param num: Number of models
        :param model_type: Class of the model
        :param init_vals: Dictionary for the initialization


        :
        init_vals = {"E_bat_rated": 5000,
                    "soc_init": 0.1,
                    "loss_rate": 0.02,
                    "DOD_max": 0.95}
        :
    
        '''
        # next entity ID
        next_eid = len(self.entities)

        # create list for entities
        entities = []

        # loop over all entities
        for i in range(next_eid, next_eid + num):
            # create name of model
            ename = '%s%s%d' % (model_type, '_', i)

            # append step size
            init_vals[i]["delta"] = self.step_size

            # create new model with init values
            model = self.simulator.add_model(model_type, init_vals[i])

            # create full id
            full_id = self.sid + '.' + ename

            # add model information
            self.entities[ename] = {
                'ename': ename,
                'etype': model_type,
                'model': model,
                'full_id': full_id}

            # append entity to list
            entities.append({'eid': ename, 'type': model_type})

        # return created entities
        return entities

    # perform simulation step
    def step(self, time, inputs, max_advance=3600):
        # loop over input signals
        for eid, attrs in inputs.items():
            for attr, values in attrs.items():
                # set value
                setattr(
                    self.entities[eid]["model"], attr, sum(
                        values.values()))

        # Perform simulation step
        self.simulator.step()

        # Next timestamp for simulation
        return time + self.step_size

    def get_data(self, outputs):
        # create output data
        data = {}

        # loop over all output values
        for ename, attrs in outputs.items():
            # get model entry
            entry = self.entities[ename]

            # set data entry for model
            data[ename] = {}

            # loop over all attributes
            for attr in attrs:
                if attr not in self.meta['models'][entry["model"].type]['attrs']:
                    raise ValueError('Unknown output attribute: %s' % attr)

                # Get model data
                data[ename][attr] = getattr(entry["model"], attr)

        # return data to mosaik
        return data

    # get entities
    def get_entities(self):
        # return entities of API
        return self.entities


def main():
    return mosaik_api.start_simulation(StorageSimulator())
