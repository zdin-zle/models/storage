"""
Model of an electrical battery storage including dynamic efficiency 
curves of battery inverter

@author: Christian Reinhold and Henrik Wagner elenia@TUBS

@review: Fernando Penaherrera @OFFIS/UOL

"""

import decimal
from decimal import Decimal
decimal.getcontext().prec = 4


class eStorage:
    """
    electric Storage-Model

    Can be created with default values or with a dictionary of initial values.

    An energy management model (e.g. mosaik_control) can be used to determine P_SET values.

    >>> eStorageModel1 = eStorage()
    >>> eStorage2 = eStorage(init_vals={"E_bat_rated": 5000,
                                        "soc_init": 0.1,
                                        "loss_rate": 0.02,
                                        "DOD_max": 0.95})
    """

    # Static properties - overwritten by model_data.json
    type = 'eStorage'               # Model type [-]
    soc_init = 0.5                  # Initial SOC [-]
    E_bat_rated = 10000             # Capacity [Wh]
    P_rated_discharge_max = 8000    # Rated discharging active power [W]
    P_rated_charge_max = 8000       # Rated charging active power [W]
    eff_discharge_init = 0.95       # static efficiency discharge [-]
    eff_charge_init = 0.95          # static efficiency charge [-]
    status_curve = False            # on/off dynamic charging [-]
    loss_rate = 0.02                # self-discharge per month [%/month]
    DOD_max = 0.95                  # max. depth of discharge [-]
    status_aging = False            # on/off consideration of reduced battery 
                                    # capacity over time (battery aging effects)
    SoH_init = 1                    # Initial SoH at t=0, 1 equals 100%
    SoH_cycles_max = 0.8            # Cycle limit of SoH, at 80% mostly battery 
                                    # turns into 2nd life
    bat_cycles_max_soh = 0.8        
    bat_cycles_max = 5000           # Max. battery cycles [-]
    bat_cycles_init = 0             # Initial battery cycles storage [-]

    # Simulation properties
    delta = 60                      # Time-Delta [s]

    # Dynamic input properties
    
    P_SET = None                    # Set-Point - Battery Active Power 
                                    # (+ Charge, - Discharge) [W]

    # Dynamic output properties
    E_BAT = None                    # Current charging level [Wh]
    P_SET_LIMIT = None              # Limitation of Set Point [W]
    SOC = None                      # Current SOC - State of Charge [-]
    P_CHARGE_MAX = None             # Current maximal active charge power [W]
    P_DISCHARGE_MAX = None          # Current maximal active discharge power [W]
    BAT_CYCLES = None               # Current amount of battery cycles [-]
    SOH = None                      # Current state of health of battery [-]

    # result properties
    results = [
        'E_BAT',
        'P_SET_LIMIT',
        'SOC',
        'P_CHARGE_MAX',
        'P_DISCHARGE_MAX',
        'BAT_CYCLES',
        'SOH']

    def __init__(self, init_vals=None):
        '''
        Class Constructor. Uses the the static properties as entry

        :param init_vals: Dictionary containing the initial values

        :
        init_vals = {"E_bat_rated": 5000,
                    "soc_init": 0.1,
                    "loss_rate": 0.02,
                    "DOD_max": 0.95}
        :

        '''
        
        # assign initial values from the init_vals dictionary
        if init_vals:
            for key, value in init_vals.items():
                setattr(self, key, value)

        # set actual SOC
        self.SOC = self.soc_init

        # calculate usable battery capacity
        self.E_bat_usable = self.E_bat_rated * self.DOD_max

        # set real charging level E-Bat
        self.E_BAT = self.SOC * self.E_bat_usable

        # calculate energy of one charge-discharge cycle
        self.E_cycle = 2 * self.E_bat_usable

        # set amount of battery cycles
        self.BAT_CYCLES = self.bat_cycles_init

        # set initial SoH
        self.SOH = self.SoH_init

        # set initial set point for active battery power value
        self.P_SET = 0

        # set initial charge/discharge efficiency
        self.eta_charge = self.eff_charge_init
        self.eta_discharge = self.eff_discharge_init

        # calculate self-discharge per timestep
        self.self_discharge = (
            self.loss_rate / (30 * 24 * 60 * 60)) * self.delta


    def _set_active_power_limit(self):
        '''
        Sets the output active power.
        Controls that it does not exceed the battery charge
        and discharge limits
        '''
        # Set Limit
        if self.P_SET > self.P_rated_charge_max:
            self.P_SET_LIMIT = self.P_rated_charge_max
        elif self.P_SET < -self.P_rated_discharge_max:
            self.P_SET_LIMIT = -self.P_rated_discharge_max
        else:
            self.P_SET_LIMIT = self.P_SET

    def _calculate_new_charging_level(self):
        '''
        Calculates the updated energy content after charging/discharging
        Uses charging efficiency levels (eta_charge / eta_discharge)
        '''
        # reset turnover/volume of energy per step
        self.E_bat_step_volume = 0
        
        # Calculate new charging level
        
        # Charging
        if self.P_SET_LIMIT >= 0:  
            self._calc_eta_charge()

            # check if storage is full
            if self.E_BAT < self.E_bat_usable:
                self.E_bat_step_volume = self.P_SET_LIMIT * \
                    (self.delta / 3600) * self.eta_charge
                self.E_BAT = self.E_BAT + self.E_bat_step_volume
                
                # Check that battery energy content is not surpassed
                if self.E_BAT > self.E_bat_usable:
                    self.E_BAT = self.E_bat_usable
        
        # Discharging
        elif self.P_SET_LIMIT < 0:  
            self._calc_eta_discharge()  # calculate dynamic discharging efficiency
            
            if self.E_BAT > 0:
                self.E_bat_step_volume = self.P_SET_LIMIT * \
                    self.delta / 3600 * self.eta_discharge
                self.E_BAT = self.E_BAT + self.E_bat_step_volume
                
                # Check that the level is above 0
                if self.E_BAT < 0:
                    self.E_BAT = 0

    def step(self):
        '''
        Advances the battery model.
        Calculates all of the Dynamic Properties based on the Static Properties.
        '''
        # Set active power limit
        self._set_active_power_limit()

        # Calculate energy loss due to self-discharge
        self.E_BAT = self.E_BAT * (1 - self.self_discharge)

        # Calculate new charging level E_BAT
        self._calculate_new_charging_level()

        # Calculate SOC
        self.SOC = min(self.E_BAT / self.E_bat_usable, 1)

        # Calculate battery cycles
        self.BAT_CYCLES = self.BAT_CYCLES + \
            abs(self.E_bat_step_volume / self.E_cycle)

        # Calculate maximum discharging active power
        if self.eta_discharge == 0:
            self.P_DISCHARGE_MAX = 0
        
        else:
            self.P_DISCHARGE_MAX = min(
                self.E_BAT * self.eta_discharge / (self.delta / 3600),
                self.P_rated_discharge_max)

        # Calculate maximum charging active power
        if self.eta_charge == 0:
            self.P_CHARGE_MAX = 0
        
        else:        
            self.P_CHARGE_MAX = min(
                (self.E_bat_usable - self.E_BAT) / self.eta_charge / (self.delta / 3600),
                self.P_rated_charge_max)

        # Calculate battery state of health and aging properties
        if self.status_aging:
            self._calculate_aging_status()

    def _calculate_aging_status(self):
        '''
        Checks if the State of Health (SoH) limit has been reached.
        '''
        
        # Check if SoH limit is already reached
        if self.SOH > (1 - self.SoH_cycles_max):
            
            # Calculate bat_aging per cycle and new SoH
            self.bat_aging = (self.SoH_cycles_max - self.SoH_init) / \
                (self.bat_cycles_max - self.BAT_CYCLES)
            self.SOH = self.SoH_init + self.bat_aging * self.BAT_CYCLES
            
            # calculate reduced E_bat_usable due to aging
            self.E_bat_usable = (
                self.E_bat_rated * self.DOD_max) * self.SOH
        
        elif self.SoH <= (1 - self.SoH_cycles_max):
            raise ValueError(
                "Warning: Minimum SoH of %s has been reached - battery needs to be replaced or SoH limit / bat_cycles_init" %
                (1 - self.SoH_cycles_max))

    def _calc_eta_discharge(self):
        '''
        Calculation of dynamic discharging efficiency (eta_BAT2AC).
        Coefficients of poliniomial function can be set according to
        inverter type with help of data/battery_inverter_data.json.
        Coefficients based on measurements of inverter types from Sunny Boy Storage 6.0 
        at elenia Institute at TU Braunschweig and Open Source Tool PerModAC
        from HTW Berlin are provided. All coefficients are based on measurments 
        which fulfill requirements of "Effizienzleitfaden für PV-Speichersysteme".
        '''
        
        if self.status_curve:
            self.eta_BAT2AC = [Decimal(a) for a in self.eta_BAT2AC]
            
            self.P_ratio = Decimal(
                abs(self.P_SET_LIMIT / self.P_rated_discharge_max))
            
            if self.P_ratio <= Decimal(0.012): # note: battery inverter does not start for low P_ratio
                self.eta_discharge = 0
            else:
                # Formula:
                # eta = a0 * (P/P_max)^a1 + a2
                self.eta_discharge = float(self.eta_BAT2AC[0] * (
                    self.P_ratio**self.eta_BAT2AC[1])+ self.eta_BAT2AC[2])

    def _calc_eta_charge(self):
        '''
        Calculation of dynamic charging efficiency (eta_AC2BAT)
        Coefficients of poliniomial function can be set according to
        inverter type with help of data/battery_inverter_data.json.
        Coefficients based on measurements of inverter types from Sunny Boy Storage 6.0 
        at elenia Institute at TU Braunschweig and Open Source Tool PerModAC
        from HTW Berlin are provided. All coefficients are based on measurments 
        which fulfill requirements of "Effizienzleitfaden für PV-Speichersysteme".
        '''
        if self.status_curve:
            self.eta_AC2BAT = [Decimal(a) for a in self.eta_AC2BAT]

            self.P_ratio = decimal.Decimal(
                abs(self.P_SET_LIMIT / self.P_rated_charge_max))
            if self.P_ratio <= Decimal(0.012):  # note: battery inverter does not start for low P_ratio
                self.eta_charge = 0
            else:
                # Formula:
                # eta = a0 * (P/P_max)^a1 + a2
                self.eta_charge = float(self.eta_AC2BAT[0] * (
                    self.P_ratio**self.eta_AC2BAT[1]) + self.eta_AC2BAT[2])
    
    def __repr__(self):
        '''String for identification of the battery'''
        
        text = "Electric Battery Storage Model" + "\n"
        text += "Capacity [Wh] = %s" % (self.E_bat_rated) + "\n"
        text += "Real Capacity [Wh] = %s" % (self.E_bat_usable) + "\n"
        text += "Maximum Charging Power [W] = %s" % (
            self.P_CHARGE_MAX) + "\n"
        text += "Maximum Discharging Power [W] = %s" % (self.P_DISCHARGE_MAX) + "\n"
        return text 

class hStorage:
    # static properties
    type = 'hStorage'

    # constructor
    def __init__(self, init_vals):

        # assign init values
        for _, (key, value) in enumerate(init_vals.items()):
            setattr(self, key, value)


class StorageModels(object):
    '''
    Contains a list with Storage Models and a result list
    Used to access information on the different entities of the model
    '''
    
    # Constructor
    def __init__(self):
        # init data elements
        self.models = []
        self.results = []

    # create model instances
    def add_model(self, etype='eStorage', init_vals=None):
        # select by type
        if etype == 'eStorage':
            # create electric storage model
            model = eStorage(init_vals)
        elif etype == 'hStorage':
            # create hydrogen storage model
            model = hStorage(init_vals)

        # add model to model storage
        self.models.append(model)

        # add list for simulation data
        self.results.append([])

        # return model
        return model

    # perform simulation step of all models
    def step(self):
        # Enumeration over all models in simulator
        for i, model in enumerate(self.models):
            # perform simulation step
            model.step()

            # collect data of model and storage local
            for j, signal in enumerate(model.results):
                self.results[i].append(getattr(model, signal))


if __name__ == "__main__":  
    eStorage1 = eStorage()
    init_vals = {"E_bat_rated": 5000,
                 "soc_init": 0.1}
    eStorage2 = eStorage(init_vals={"E_bat_rated": 5000,
                                    "soc_init": 0.2,
                                    "loss_rate": 0.02,
                                    "DOD_max": 0.95})

    print(eStorage1)
    print(eStorage2)

    eStorage2.P_SET = 1000
    eStorage2.delta = 10 * 60
    for _ in range(12 * 2 * 3):
        eStorage2.step()
        #print(eStorage2.SOC)
        print(eStorage2.eta_charge)
