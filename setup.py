'''
Created on 08.10.2021

@author: Henrik Wagner elenia/TU Braunschweig
'''

# Always prefer setuptools over distutils

from setuptools import setup, find_packages
import pathlib
here = pathlib.Path(__file__).parent.resolve()

# Get the long description from the README file
long_description = (here / 'README.md').read_text(encoding='utf-8')

# Arguments marked as "Required" below must be included for upload to PyPI.
# Fields marked as "Optional" may be commented out.

setup(
    name='mosaik-storage',  # Required
    version='0.1',  # Required
    description='A simple storage model (electrical & hydrogen) running with the co-simulation framework mosaik',  # Optional
    #long_description=long_description,  # Optional
    #long_description_content_type='text/markdown',  # Optional (see note above)
    url='https://gitlab.com/zdin-zle/models/storage',  # Optional
    author='ZDIN-ZLE: Henrik Wagner',  # Optional
    author_email='henrik.wagner@tu-braunschweig.de',  # Optional
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Intended Audience :: Researchers',
        'Topic :: Software Development :: Build Tools',
        'License :: OSI Approved :: MIT License',

    ],

    keywords='Storage, Battery, Mosaik, Adapter',  # Optional
    packages=find_packages(
        where="src",
        include=["mosaik*"]

    ),
    package_dir={"": "src"},
    python_requires='>=3.6, <4',
    install_requires=[
        "arrow==1.1.1",
        "mosaik>=3.0"
        ],

    project_urls={  # Optional
        'Bug Reports': 'https://gitlab.com/zdin-zle/models/storage/-/issues',
        'Funding': 'ZDin-ZLE Funders',
        'Documentation': "https://zdin.de/zukunftslabore/energie",
        'Source': "https://gitlab.com/zdin-zle/models/storage",
    },
)


if __name__ == '__main__':
    pass
